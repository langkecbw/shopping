import request from '@/utils/request'

// 获取用户个人信息
export const getUserInfoDetail = () => {
  return request.get('/user/info')
}

// 获取 我的-余额，积分，优惠券
export const getUserAssets = () => {
  return request.get('/user/assets')
}
