import request from '@/utils/request'

// 获取地区数据
export const getRegionTree = () => {
  return request.get('/region/tree')
}

// 添加收货地址
export const setAddressAdd = (obj) => {
  const { form } = obj
  return request.post('/address/add', {
    form
  })
}

// 设置默认地址
export const setDefault = (addressId) => {
  return request.post('/address/setDefault', {
    addressId// 地址id
  })
}
