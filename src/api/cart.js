import request from '@/utils/request'

// 加入购物车
export const cartAdd = (goodsId, goodsNum, goodsSkuId) => {
  return request.post('/cart/add', {
    goodsId, // 商品ID
    goodsNum, // 商品数量
    goodsSkuId// 商品SKUID
  })
}

// 获取购物车数据
export const getCartList = () => {
  return request.get('/cart/list')
}

// 购物车商品更新
export const getUpData = (goodsId, goodsNum, goodsSkuId) => {
  return request.post('/cart/update', {
    goodsId,
    goodsNum,
    goodsSkuId
  })
}

// 删除购物车商品
export const clearcart = (cartIds) => {
  return request.post('/cart/clear', { cartIds })
}

// 获取购物车总数量
export const getCartTotal = () => {
  return request.get('/cart/total')
}
