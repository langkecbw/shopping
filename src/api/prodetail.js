import request from '@/utils/request'

// 获取商品详情数据
export const getProDetail = (goodsId) => {
  return request.get('/goods/detail', {
    params: {
      goodsId
    }
  })
}

// 获取商品保障服务
export const getGoodsService = (goodsId) => {
  return request.get('/goods.service/list', {
    params: {
      goodsId
    }
  })
}

// 获取商品评论总数
export const getProDetailtotal = (goodsId) => {
  return request.get('/comment/total', {
    params: {
      goodsId
    }
  })
}

// 获取商品评论
export const getProDetailListRows = (limit, goodsId) => {
  return request.get('/comment/listRows', {
    params: {
      goodsId,
      limit// 获取评论数
    }
  })
}

// 获取评价列表
export const getProDetailList = (scoreType, goodsId) => {
  return request.get('/comment/list', {
    params: {
      scoreType,
      goodsId,
      page: 1
    }
  })
}
