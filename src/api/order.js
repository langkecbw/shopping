import request from '@/utils/request'

// 订单结算
export const checkOrder = (mode, obj) => {
  return request.get('/checkout/order', {
    params: {
      mode, // 结算模式（buyNow立即购买 cart购物车
      delivery: 10, // 配送方式（10快递配送 20上门自提）
      couponId: 0, // 优惠券ID
      isUsePoints: 0, // 是否使用积分抵扣（1使用 0不使用）
      ...obj// cartIds购物车ID集; shopId自提门店ID ;goodsSkuId
    }
  })
}

// 提交订单
// mode:cart   => obj{ cartIds, remark }
// mode:buyNow => obj{ goodsId, goodsNum, goodsSKuId, remark }
export const submitOrder = (mode, obj) => {
  return request.post('/checkout/submit', {
    mode,
    delivery: 10, // 配送方式（10快递配送 20上门自提）
    couponId: 0, // 优惠券ID
    isUsePoints: 0, // 是否使用积分抵扣（1使用 0不使用）
    payType: 10, // 支付方式，10：余额支付
    ...obj

  })
}

// 获取订单信息
export const getOrderList = (dataType, page) => {
  return request.get('/order/list', {
    params: {
      dataType, // 订单类型，all-全部，payment-待支付，delivery-待发货，received-待收货，comment-待评价
      page
    }
  })
}

// 取消订单
export const orderCancel = (orderId) => {
  return request.post('/order/cancel', {
    orderId// 订单id
  })
}
