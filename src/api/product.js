import request from '@/utils/request'

// 搜索
export const getProList = (obj) => {
  const { sortType, sortPrice, categoryId, goodsName, page } = obj
  return request.get('/goods/list', {
    params: {

      sortType, // all-按综合搜索(默认)，sales-按销量搜索，price-按价格搜索
      sortPrice, // 0-价格从低到高， 1-价格从高到低
      categoryId, // 默认值
      goodsName, // 商品名称
      page// 页码，默认值：1
    }
  })
}
