import { clearcart, getCartList, getUpData } from '@/api/cart'
import { Toast } from 'vant'

export default {
  namespaced: true,
  state () {
    return {
      cartList: []
    }
  },
  mutations: {
    setCartList (state, newCart) {
      state.cartList = newCart
    },

    // 单选
    toggleCheck (state, goodsId) {
      const goods = state.cartList.find(item => item.goods_id === goodsId)
      goods.isChecked = !goods.isChecked
    },

    // 全选
    toggleAllCheck (state, flag) {
      state.cartList.forEach(item => (item.isChecked = flag))
    },

    // 数字框
    changeCount (state, { goodsId, goodsNum }) {
      const obj = state.cartList.find(item => item.goods_id === goodsId)
      obj.goods_num = goodsNum
    }
  },
  actions: {
    // 获取数据
    async getCartAction (context) {
      const { data } = await getCartList()
      // console.log(data)
      data.list.forEach(item => {
        item.isChecked = true
      })
      context.commit('setCartList', data.list)
    },

    // 数字框
    async changeCountAction (context, obj) {
      const { goodsId, goodsNum, goodsSkuId } = obj
      context.commit('changeCount', { goodsId, goodsNum })
      await getUpData(goodsId, goodsNum, goodsSkuId)
    },

    // 删除购物车商品
    async delSelect (context) {
      const selCartList = context.getters.selCartList
      const cartIds = selCartList.map(item => item.id)
      await clearcart(cartIds)
      context.dispatch('getCartAction')
      Toast('已删除')
    }
  },
  getters: {
    // 商品总数
    cartTotal (state) {
      return state.cartList.reduce((sum, item) => sum + item.goods_num, 0)
    },

    // 选中的商品
    selCartList (state) {
      return state.cartList.filter(item => item.isChecked)
    },

    // 选中的总数
    selCount (state, getters) {
      return getters.selCartList.reduce((sum, item) => sum + item.goods_num, 0)
    },

    // 选中的总价
    selPrice (state, getters) {
      return getters.selCartList.reduce((sum, item) => sum + item.goods.goods_price_min * item.goods_num, 0)
    },

    // 全选
    // isAllChecked (state, getters) {
    //   if (getters.selCount === getters.cartTotal) {
    //     state.allChecked = true
    //   } else {
    //     state.allChecked = false
    //   }
    // }
    isAllChecked (state) {
      return state.cartList.every(item => item.isChecked)
    }
  }
}
