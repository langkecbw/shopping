import Vue from 'vue'

// 按需引入
import { Tabbar, TabbarItem, Button, NavBar, Toast, Search, Swipe, SwipeItem, Grid, GridItem, Icon, Popup, Rate, ActionSheet, Dialog, Checkbox, Badge, Empty, AddressList, AddressEdit, Tab, Tabs } from 'vant'

Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(Button)
Vue.use(NavBar)
Vue.use(Toast)
Vue.use(GridItem)
Vue.use(Search)
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Grid)
Vue.use(Icon)
Vue.use(Popup)
Vue.use(Rate)
Vue.use(ActionSheet)
Vue.use(Dialog)
Vue.use(Checkbox)
Vue.use(Badge)
Vue.use(Empty)
Vue.use(AddressList)
Vue.use(AddressEdit)
Vue.use(Tab)
Vue.use(Tabs)
