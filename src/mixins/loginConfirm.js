
export default {
  // 此处编写的就是 Vue 组件实例的 配置项， 通过一定的语法，可以直接混入到组件内部
  // data methods computed 生命周期函数 ... 都可以混入到组件内部
  // 注意点：如果此处 和 组件内，提供了同名的 data 或 methods， 则组件内优先级更高
  methods: {
    loginConfirm () {
      // 根据登录状态，判断是否要弹出确认框
      // 已登录 => 不弹出，无事发生
      // 未登录 => 弹出确认框。
      if (!this.$store.getters.token) {
        this.$dialog.confirm({
          title: '温馨提示',
          message: '此时需要先登录才能继续操作哦',
          confirmButtonText: '去登录',
          cancelButtonText: '再逛逛'
          // closeOnPopstate: true
        })
          .then(() => {
            // 跳转到登录，登录会跳转回来
            this.$router.push({
              path: '/login',
              query: {
                backUrl: this.$route.fullPath
              }
            })
          })
          .catch(() => {})
        return true
      }
      return false
    }
  }
}
